##Distributīvās sistēmas

###Individuāli radīts video tēmai Distributīvās Sistēmas, RTU ETF REBC01 "Komunikācijas distributīvās sistēmas (RAE359)" kursa ietvaros.

#### _Materiāla saturs:_  īss izklāsts, par distributīvo sistēmu pamatprincipu, priekšrocībām un trūkumiem, valoda - angļu.
#### Šajā [ Video ](https://www.youtube.com/watch?v=V360CJUM-QU) redzams mans kolēģis, kurš piekrita iepazīstināt skatītāju sīkā izklāstā par augstāk minēto tēmu.

![Alt text](http://www.simpletechture.nl/img/Figure1.png)


## Distributed System
>
> __What does it mean?__
> #### A distributed system is a network that consists of autonomous computers that are connected using a distribution middleware. 
> #### They help in sharing different resources and capabilities to provide users with a single and integrated coherent network.

__The key features of a distributed system are:__
####  Components in the system are concurrent. A distributed system allows resource sharing, including software by systems connected to the network at the same time.
####  The components could be multiple but will generally be autonomous in nature.
####  A global clock is not required in a distributed system. The systems can be spread across different geographies.
####  Compared to other network models, there is greater fault tolerance in a distributed model.
####  Price/performance ratio is much better.

__The key goals of a distributed system include:__
> ####  **Transparency:** Achieving the image of a single system image without concealing the details of the location, access, migration, concurrency, failure, relocation, persistence and resources to the users.
> ####  **Openness:** Making the network easier to configure and modify.
> ####  **Reliability:** Compared to a single system, a distributed system should be highly capable of being secure, consistent and have a high capability of masking errors.
> ####  **Performance:** Compared to other models, distributed models are expected to give a much-wanted boost to performance.
> ####  **Scalability:** Distributed systems should be scalable with respect to geography, administration or size.

__Challenges for the distributed system include:__
####  Security is a big challenge in a distributed environment, especially when using public networks.
####  Fault tolerance could be tough when the distributed model is built based on unreliable components.
####  Coordination and resource sharing can be difficult if proper protocols or policies are not in place.
####  Process knowledge should be put in place for the administrators and users of the distributed model.


__Examples of distributed systems__
> #### Intranets, Internet, WWW, email, ...
> #### DNS (Domain Name Server) - Hierarchical distributed database
> #### Distributed supercomputers, Grid/Cloud computing
> #### Electronic banking
> #### Airline reservation systems
> #### Peer-to-peer networks
> #### Sensor networks
> #### Mobile and Pervasive Computing


